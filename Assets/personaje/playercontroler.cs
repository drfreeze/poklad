using UnityEngine;
using System.Collections;

public class playercontroler : MonoBehaviour {

	public float altura_salto;
	public float velocidad_movimiento;
	private Rigidbody2D rb;
	private Animator anim;
	private bool toco_piso;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		anim.SetInteger ("estado", 0);
	}

	void OnCollisionEnter2D(Collision2D c)
	{
		toco_piso = c.gameObject.tag.Equals("piso");
    }


	// Update is called once per frame
	void Update () {
  		if (toco_piso)
		{
			anim.SetInteger("estado", 0);
		}
		if(Input.GetKey(KeyCode.Space) && toco_piso)
		{
			rb.velocity = new Vector2 (rb.velocity.x, altura_salto);
			toco_piso = false;
			anim.SetInteger ("estado",2);
		}
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			rb.velocity = new Vector2 (-velocidad_movimiento, rb.velocity.y);
			rb.transform.localScale = new Vector2 (-1, 1);
			anim.SetInteger ("estado",1);
		}
		if (Input.GetKey(KeyCode.RightArrow))
		{
			rb.velocity = new Vector2 (velocidad_movimiento, rb.velocity.y);
			rb.transform.localScale = new Vector2 (1, 1);
			anim.SetInteger ("estado",1);
		}
	}
}
